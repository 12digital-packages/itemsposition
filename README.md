# Item position

##### A package for managing the order of multiple related items of a model.

## Usage

1. Implement ItemsPositionable interface
    ```php
    class Column extends Model implements ItemsPositionable {}
    ```

2. Add ItemsPositionTrait to model
    ```php
    use ItemsPositionTrait;
    ```

3. Inside model boot method
    ```php
   self::onCreateWatcher(); // create iniposition instance for model
    ```
   
4. You can get ItemsPosition model
    ```php
    $itemsPos = $model->itemsPosition;
    ```
   
5. Add additional attribute for get ordered id array of items
    ```php
    public function getItemsOrderAttribute()
    {
        return $this->itemsPosition->position_array;
    }
    ```
   
6. Change items order
    
    Method for change items order
    ```php
    positionChanged(int $moveType, int $itemId, ?int $newPos = null);
    ```
   
    MoveTypes
    ```php
    PositionChangeType::IN_ONE; // change position inside one model (swap ids)
    PositionChangeType::FROM; // moved from model (remove from order array)
    PositionChangeType::TO; // moved to model (add id to order array)
    ```
   
   Examples
   ```php
    $fromColumnInstance->positionChanged(PositionChangeType::FROM, $item->id);
    $toColumnInstance->positionChanged(PositionChangeType::TO, $item->id, $newIndex);
    ```