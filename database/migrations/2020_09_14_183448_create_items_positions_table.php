<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_positions', function (Blueprint $table) {
//            $table->uuid('id')->primary();
            $table->id();
            $table->string('model_type');
//            $table->string('items_model_type');
//            $table->string('items_relation_name')->nullable();
            $table->unsignedBigInteger('model_id');
            $table->text('position_array')->nullable();
            $table->index(['model_id', 'model_type'], 'model_has_items_positions_model_id_model_type_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_positions');
    }
}
