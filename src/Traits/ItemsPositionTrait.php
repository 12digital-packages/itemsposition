<?php

namespace Digital12\ItemsPosition\Traits;

use Digital12\ItemsPosition\Events\PositionChanged;
use Digital12\ItemsPosition\Models\ItemsPosition;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Trait ItemsPositionTrait
 * For models which implements ItemsPositionable
 * @package Digital12\ItemsPosition\Traits
 */
trait ItemsPositionTrait
{
    /**
     * To model::boot() method
     */
    public static function onCreateWatcher()
    {
        self::created(function($model) {
            $model->itemsPosition()->create();
        });
    }

    /**
     * @return MorphOne
     */
    public function itemsPosition() :MorphOne
    {
        return $this->morphOne(ItemsPosition::class, '', 'model_type', 'model_id');
    }

    /**
     * Triggers position change event
     * @param int $moveType
     * @param int $itemId
     * @param int|null $newPos
     */
    public function positionChanged(int $moveType, int $itemId, ?int $newPos = null)
    {
        event(new PositionChanged($moveType, get_class($this), $this->id, $itemId, $newPos));
    }

}
