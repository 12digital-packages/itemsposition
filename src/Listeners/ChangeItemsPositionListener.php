<?php

namespace Digital12\ItemsPosition\Listeners;

use Digital12\ItemsPosition\Events\PositionChanged;
use Digital12\ItemsPosition\Services\ItemsPositionsService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ChangeItemsPositionListener
{
    private ItemsPositionsService $itemsPositionService;

    /**
     * Create the event listener.
     *
     * @param ItemsPositionsService $itemsPositionService
     */
    public function __construct(ItemsPositionsService $itemsPositionService)
    {
        $this->itemsPositionService = $itemsPositionService;
    }

    /**
     * Handle the event.
     *
     * @param  PositionChanged  $event
     * @return void
     */
    public function handle(PositionChanged $event)
    {
        $this->itemsPositionService->positionChanged($event->moveType,$event->itemsPosition, $event->itemId, $event->newPos);
    }
}
