<?php

namespace Digital12\ItemsPosition\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @todo Class for ordered children implementation (such as columns now)
 * One logic for tasks, columns, desks, etc.
 * Class ItemsPosition
 * @package App\Models
 */
class ItemsPosition extends Model
{
    public $timestamps = false;

    protected $casts = ['position_array' => 'array'];

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        static::created(fn($model) => $model->initPositionArray()->save());
    }

    public function model(): MorphTo
    {
        return $this->morphTo('', 'model_type', 'model_id');
    }

    public function initPositionArray(): ItemsPosition
    {
        $this->position_array = $this->model->initPositionArray();
        return $this;
    }
}
