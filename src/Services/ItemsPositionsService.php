<?php

namespace Digital12\ItemsPosition\Services;

use Digital12\ItemsPosition\Enums\PositionChangeType;
use Digital12\ItemsPosition\Models\ItemsPosition;

class ItemsPositionsService
{
    public function positionChanged(int $moveType, ItemsPosition $itemsPosition, int $itemId, ?int $newPos)
    {
        $position_array = $itemsPosition->position_array;

        if ($position_array === null) {
            $position_array = $itemsPosition->initPositionArray()->position_array;
        }

        switch ($moveType) {
            // if task moved in one column
            case PositionChangeType::IN_ONE:
                $index = array_search($itemId, $position_array); // $oldPos
                array_splice($position_array, $index, 1, []);
                array_splice($position_array, $newPos, 0, [$itemId]);
                break;

            // if task moved from column
            case PositionChangeType::FROM:
                $index = array_search($itemId, $position_array); // $oldPos
                array_splice($position_array, $index, 1, []);
                break;

            // if task moved to column
            case PositionChangeType::TO:
                array_splice($position_array, $newPos, 0, [$itemId]);
                break;
        }
        $itemsPosition->position_array = $position_array;
        return $itemsPosition->save();
    }
}
