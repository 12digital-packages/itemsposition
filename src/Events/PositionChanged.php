<?php

namespace Digital12\ItemsPosition\Events;

use Digital12\ItemsPosition\Models\ItemsPosition;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PositionChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public int $moveType;
    public ItemsPosition $itemsPosition;
    public int $itemId;
    public ?int $newPos;

    /**
     * Create a new event instance.
     *
     * @param int $moveType
     * @param string $model
     * @param int $id
     * @param int $itemId
     * @param int|null $newPos
     */
    public function __construct(int $moveType, string $model, int $id, int $itemId, ?int $newPos = null)
    {
        $this->moveType = $moveType;
        $this->itemsPosition = ItemsPosition::firstOrCreate(['model_type' => $model, 'model_id' => $id]);
        $this->itemId = $itemId;
        $this->newPos = $newPos;
    }

//    /**
//     * Get the channels the event should broadcast on.
//     *
//     * @return \Illuminate\Broadcasting\Channel|array
//     */
//    public function broadcastOn()
//    {
//        return new PrivateChannel('channel-name');
//    }
}
