<?php

namespace Digital12\ItemsPosition\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static IN_ONE()
 * @method static static FROM()
 * @method static static TO()
 */
final class PositionChangeType extends Enum
{
    const IN_ONE = 1;
    const FROM = 2;
    const TO = 3;
}
