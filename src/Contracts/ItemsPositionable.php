<?php

namespace Digital12\ItemsPosition\Contracts;

use Illuminate\Database\Eloquent\Relations\MorphOne;

interface ItemsPositionable
{
    public function initPositionArray(): array;

    public function itemsPosition(): MorphOne;

    public function positionChanged(int $moveType, int $itemId, ?int $newPos = null);

}
