<?php

namespace Digital12\ItemsPosition\Providers;

use Digital12\ItemsPosition\Events\PositionChanged;
use Digital12\ItemsPosition\Listeners\ChangeItemsPositionListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        PositionChanged::class => [
            ChangeItemsPositionListener::class
        ]
    ];

    public function boot()
    {
        parent::boot();
    }
}
